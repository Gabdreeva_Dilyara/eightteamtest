package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.Question;
import ru.itis.models.Subject;
import ru.itis.models.User;
import ru.itis.repositories.QuestionRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class QuestionService {

    private static String UPLOADED_FOLDER = "C:\\Users\\Диляра\\IdeaProjects\\EightTeamPr\\src\\main\\resources\\static\\img\\";

    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private SubjectService subjectService;

    public List<Question> getQuestions(String subject) {
        return questionRepository.getAllByQSubject(subjectService.getSubjectByName(subject));
    }

    public void saveQuestion(Question question, User user, String path) {
        Subject subject = subjectService.getSubjectByNameRus(question.getSubjectName());
        System.out.println(subject.getName());
        Set<Question> questions = subject.getQuestionsSet();
        questions.add(question);
        subject.setQuestionsSet(questions);
        question.setUser(user);
        question.setQSubject(subject);
        if (!path.equals("")) {
            question.setQFile(path);
        }
        questionRepository.save(question);
        subjectService.saveSubject(subject);
    }

    public String saveFile(MultipartFile file){
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
            return path.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Question getQuestionById(Long id) {
        return questionRepository.findOne(id);
    }
}
