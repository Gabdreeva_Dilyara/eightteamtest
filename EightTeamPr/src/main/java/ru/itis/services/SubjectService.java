package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.Subject;
import ru.itis.repositories.SubjectRepository;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    public List<Subject> getSubjects() {
        return subjectRepository.findAll();
    }
    public Subject getSubjectByName(String name) {
        return subjectRepository.findByEngName(name);
    }
    public Subject getSubjectByNameRus(String name) {
        return subjectRepository.findByName(name);
    }
    public void saveSubject(Subject subject) {
        subjectRepository.save(subject);
    }

}
