package ru.itis.services;

import org.springframework.security.core.Authentication;
import ru.itis.models.User;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */

public interface AuthenticationService {
    User getUserByAuthentication(Authentication authentication);
}
