package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.forms.MaterialForm;
import ru.itis.models.Subject;
import ru.itis.models.SubjectData;
import ru.itis.repositories.SubjectDataRepository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class MaterialService {
    private static final String UPLOADED_FOLDER = "C:\\Users\\Диляра\\IdeaProjects\\EightTeamPr\\src\\main\\resources\\static\\docs\\";
    @Autowired
    private SubjectDataRepository materialRepository;

    @Autowired
    private SubjectService subjectService;

    public List<SubjectData> getMaterial(String subject) {
        List<SubjectData> list = materialRepository.getAllBySubject(subjectService.getSubjectByName(subject));
        return list;
    }
    public void saveMaterial(SubjectData material, String name, String path, MaterialForm form){
        Subject subject = subjectService.getSubjectByName(name);
        Set<SubjectData> materials;
        if (subject.getSubjectDataSet() == null) {
            materials = new HashSet<>();
        }
        else {
            materials = subject.getSubjectDataSet();
        }
        materials.add(material);
        subject.setSubjectDataSet(materials);
        material.setSubject(subject);
        material.setPath(path);
        material.setContent(form.getContent());
        material.setHeader(form.getHeader());
        materialRepository.save(material);
        subjectService.saveSubject(subject);
    }
    public String saveFile(MultipartFile file){
        try {
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);
            return path.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}

