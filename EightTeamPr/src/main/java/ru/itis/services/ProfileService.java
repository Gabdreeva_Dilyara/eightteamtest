package ru.itis.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.Question;
import ru.itis.models.User;
import ru.itis.repositories.QuestionRepository;
import ru.itis.repositories.UserRepository;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Service
public class ProfileService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuestionRepository questionRepository;

    public User getUser(Long id){
        return userRepository.getOne(id);
    }

    public List<Question> getAllQuestions(User user) {
        return questionRepository.findAllByUser(user);
    }
}
