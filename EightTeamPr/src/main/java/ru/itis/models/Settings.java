package ru.itis.models;

import lombok.*;

import javax.persistence.*;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "settings")
public class Settings {

    @Id
    @Column(name = "settings_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "show_name")
    private boolean showName;

    @Column(name = "show_last_name")
    private boolean showLastName;

    @Column(name = "show_group")
    private boolean showGroup;
    }
