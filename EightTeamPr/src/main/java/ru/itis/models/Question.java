package ru.itis.models;

import lombok.*;
import org.hibernate.annotations.Type;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.io.File;
import java.util.Date;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Entity
@Table(name = "question")
public class Question {
    @Id
    @Column(name = "question_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long questionId;

    @Column(name = "header")
    private String header;

    @Column(name = "content")
    private String content;

    @Column(name = "file")
    private String qFile;

    @Transient
    private String subjectName;

    @Column(name = "complaintscount")
    private int complaintsCount;

    @Column(name = "datetime")
    @Type(type = "timestamp")
    private Date datetime;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "subject")
    private Subject qSubject;

    @OneToOne(fetch=FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name="author")
    private User user;
}
