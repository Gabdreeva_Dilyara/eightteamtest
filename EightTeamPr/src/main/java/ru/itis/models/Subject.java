package ru.itis.models;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "subject")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "subject_id")
    private Long subjectId;

    @Column(name = "name")
    private String name;

    @Column(name = "eng_name")
    private String engName;

    @OneToMany(mappedBy = "subject")
    private Set<SubjectData> subjectDataSet = new HashSet<>();

    @OneToMany(mappedBy = "qSubject")
    private Set<Question> questionsSet = new HashSet<>();

}
