package ru.itis.models;

import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
@Entity
public class Answer {

    @Id
    @GeneratedValue
    @Column(name = "answer_id")
    private Long answerId;

    @Column(name = "content")
    private String content;

    @Column(name = "file")
    private String file;

    @Column(name = "rating")
    private int rating;

    @Column(name = "datetime")
    @Type(type = "timestamp")
    private Date date;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "author")
    private User user;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "question")
    private Question question;
}
