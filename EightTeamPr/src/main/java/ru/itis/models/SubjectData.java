package ru.itis.models;

import lombok.*;

import javax.persistence.*;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@Entity
@Table(name = "subject_data")
public class SubjectData {
    @Id
    @Column(name = "data_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long dataId;

    @Column(name = "header")
    private String header;

    @Column(name = "content")
    private String content;

    @Column(name = "rating")
    private int rating;

    @Column(name = "type")
    private boolean type;

    @Column(name = "path")
    private String path;

    @ManyToOne(fetch=FetchType.LAZY, cascade=CascadeType.MERGE)
    @JoinColumn(name = "subject")
    private Subject subject;

}
