package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.models.User;
import ru.itis.services.AuthenticationService;
import ru.itis.services.UserService;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */

@Controller
public class SettingsController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private UserService userService;

    @GetMapping("/setting")
    public String getSetting() {
        return "settings_page";
    }

    @PostMapping("/setting")
    public String postSetting(@ModelAttribute("settingForm") User user, Authentication authentication) {
        User currentUser = authenticationService.getUserByAuthentication(authentication);
        userService.updateUser(user, currentUser);
        return "redirect:/main";
    }
}
