package ru.itis.controllers;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class AuthController {


    @GetMapping("/login")
    public String login(@ModelAttribute("model") ModelMap model, Authentication authentication) {
        if (authentication != null) {
            return "redirect:/main";
        }
        return "login";
    }
    @GetMapping("/login/failure")
    public String loginFailure(@ModelAttribute("model") ModelMap model) {
        model.addAttribute("error", "Логин или пароль введены неверно");
        return "login";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/login";
    }

    @GetMapping("/")
    public String root(Authentication authentication, @ModelAttribute("model") ModelMap model) {
        if (authentication != null) {
            return "redirect:/main";
        }
        return "redirect:/login";
    }

}
