package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.models.Question;
import ru.itis.models.Subject;
import ru.itis.services.AuthenticationService;
import ru.itis.services.QuestionService;
import ru.itis.services.SubjectService;
import ru.itis.validators.AskFormValidator;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class AskController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private SubjectService subjectService;

    @Autowired
    private QuestionService questionService;

    @Autowired
    private AskFormValidator askFormValidator;

    @InitBinder("askForm")
    public void initAddMaterialValidator(WebDataBinder binder) {
        binder.addValidators(askFormValidator);
    }


    @GetMapping(value = "/ask")
    public String getAsk(@ModelAttribute("model") ModelMap model) {
        List<Subject> list = subjectService.getSubjects();
        model.addAttribute("subjects", list);
        return "ask";
    }

    @PostMapping(value = "/ask")
    public String postAsk(@ModelAttribute("askForm") Question question, Authentication authentication, @RequestParam("file") MultipartFile file) {
        String path = "";
        if (!file.isEmpty()) {
             path= questionService.saveFile(file);
        }
        questionService.saveQuestion(question, authenticationService.getUserByAuthentication(authentication), path);
        return "redirect:/main";
    }
}
