package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.itis.models.Question;
import ru.itis.services.QuestionService;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class QuestionController {

    @Autowired
    private QuestionService questionService;

    @GetMapping(value = "/question/{id}")
    public String getQuestion(@PathVariable("id")String id, @ModelAttribute("model")ModelMap model) {
        Question question = questionService.getQuestionById(Long.parseLong(id, 10));
        model.addAttribute("question", question);
        return "send_answer";
    }

    @PostMapping(value = "/question/{id}")
    public String postQuestion(@PathVariable("id")String id, @ModelAttribute("model")ModelMap model){
        Question question = questionService.getQuestionById(Long.parseLong(id, 10));
        String s = question.getQSubject().getEngName();
        return "redirect:/main/" + s;
    }
}
