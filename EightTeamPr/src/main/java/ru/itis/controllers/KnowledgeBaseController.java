package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.models.Subject;
import ru.itis.models.SubjectData;
import ru.itis.services.AuthenticationService;
import ru.itis.services.MaterialService;
import ru.itis.services.SubjectService;

import java.util.HashMap;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class KnowledgeBaseController {

    @Autowired
    private AuthenticationService service;

    @Autowired
    private SubjectService subjectServices;

    @Autowired
    private MaterialService materialService;


    @RequestMapping(value = "/knowledge-base")
    public String getKnowledgeBase(Authentication authentication, @ModelAttribute("model") ModelMap model) {
        List<Subject> list = subjectServices.getSubjects();
        model.addAttribute("user", service.getUserByAuthentication(authentication));
        model.addAttribute("subjects", list);
        return "knowledge_base";
    }


    @GetMapping("/knowledge-base/{subject}")
    public String getProfilePage(Authentication authentication, @ModelAttribute("model") ModelMap model, @PathVariable("subject") String subject) {
        List<Subject> list = subjectServices.getSubjects();
        model.addAttribute("user", service.getUserByAuthentication(authentication));
        model.addAttribute("subjects", list);
        HashMap<SubjectData, Subject> map = new HashMap<>();
        for (Subject s: list) {
            List<SubjectData> materialList = materialService.getMaterial(s.getEngName());
            if (materialList != null) {
                for (SubjectData material : materialList) {
                    map.put(material, s);
                }
            }
        }
        model.addAttribute("materials", map);
        model.addAttribute("currentSubject", subject);
        return "knowledge_base";
    }
}
