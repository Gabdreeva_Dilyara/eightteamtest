package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import ru.itis.models.Question;
import ru.itis.models.User;
import ru.itis.services.AuthenticationService;
import ru.itis.services.ProfileService;

import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class ProfileController {

    @Autowired
    private AuthenticationService service;

    @Autowired
    private ProfileService profileService;

    @GetMapping("/profile")
    public String getProfilePage(Authentication authentication, @ModelAttribute("model") ModelMap model) {
        User user = service.getUserByAuthentication(authentication);
        model.addAttribute("user", user);
        List<Question> questions = profileService.getAllQuestions(user);
        model.addAttribute("questions", questions);
        return "profile";
    }
}
