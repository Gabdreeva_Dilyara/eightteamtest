package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.models.Question;
import ru.itis.models.Subject;
import ru.itis.services.AuthenticationService;
import ru.itis.services.QuestionService;
import ru.itis.services.SubjectService;

import java.util.HashMap;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class MainController {

    @Autowired
    private AuthenticationService service;

    @Autowired
    private SubjectService subjectServices;

    @Autowired
    private QuestionService questionService;

    @GetMapping("/main")
    public String getProfilePage(Authentication authentication, @ModelAttribute("model") ModelMap model) {
        List<Subject> list = subjectServices.getSubjects();
        model.addAttribute("user", service.getUserByAuthentication(authentication));
        model.addAttribute("subjects", list);
        return "main";
    }
    @GetMapping("/main/{subject}")
    public String getProfilePage(Authentication authentication, @ModelAttribute("model") ModelMap model, @PathVariable("subject") String subject) {
        List<Subject> list = subjectServices.getSubjects();
        model.addAttribute("user", service.getUserByAuthentication(authentication));
        model.addAttribute("subjects", list);
        HashMap<Question, Subject> map = new HashMap<>();
        for (Subject s: list) {
            List<Question> questionList = questionService.getQuestions(s.getEngName());
            if (questionList != null) {
                for (Question question : questionList) {
                    map.put(question, s);
                }
            }
        }
        model.addAttribute("questions", map);
        model.addAttribute("currentSubject", subject);
        return "index";
    }

}
