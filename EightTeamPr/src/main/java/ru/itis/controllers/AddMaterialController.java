package ru.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.forms.MaterialForm;
import ru.itis.models.SubjectData;
import ru.itis.services.MaterialService;
import ru.itis.validators.AddMaterialValidator;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Controller
public class AddMaterialController {

    @Autowired
    private MaterialService materialService;

    @Autowired
    private AddMaterialValidator addMaterialValidator;

    @InitBinder("materialForm")
    public void initAddMaterialValidator(WebDataBinder binder) {
        binder.addValidators(addMaterialValidator);
    }

    @GetMapping(value = "/add/{subject}")
    public String getAddMaterial(@PathVariable("subject") String subject, @ModelAttribute("model")ModelMap model) {
        model.addAttribute("subject", subject);
        return "add_material";
    }

    @PostMapping(value = "/add/{subject}")
    public String postAddMaterial(@ModelAttribute("materialForm") MaterialForm form, @RequestParam("file") MultipartFile file, @PathVariable("subject") String subject) {
        SubjectData material = new SubjectData();
        String path;
        if (form.getPath().equals("")) {
            path = materialService.saveFile(file);
            material.setType(true);
        }
        else {
            path = form.getPath();
            material.setType(false);
        }
        materialService.saveMaterial(material, subject, path, form);
        return "redirect:/knowledge-base";
    }
}