package ru.itis.dao;

import ru.itis.hibernate.HibernateConnector;
import ru.itis.models.Answer;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class AnswerDaoImpl extends SaveOrUpdateImpl<Answer, Long> implements CrudDao<Answer, Long>{

    private EntityManager entityManager;

    public AnswerDaoImpl() {}

    @Override
    public Answer find(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Answer result =  entityManager.find(Answer.class, id);
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void delete(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Answer a = entityManager.find(Answer.class, id);
        entityManager.remove(a);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public List<Answer> findAll() {
        TypedQuery<Answer> query = entityManager.createQuery("SELECT w FROM Answer w", Answer.class);
        return query.getResultList();

    }
}
