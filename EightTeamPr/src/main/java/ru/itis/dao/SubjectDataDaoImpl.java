package ru.itis.dao;


import ru.itis.hibernate.HibernateConnector;
import ru.itis.models.SubjectData;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class SubjectDataDaoImpl extends SaveOrUpdateImpl<SubjectData, Long>implements CrudDao<SubjectData, Long>{

    private EntityManager entityManager;

    public SubjectDataDaoImpl() {}

    @Override
    public SubjectData find(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        SubjectData result =  entityManager.find(SubjectData.class, id);
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void delete(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        SubjectData s = entityManager.find(SubjectData.class, id);
        entityManager.remove(s);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<SubjectData> findAll() {
        TypedQuery<SubjectData> query = entityManager.createQuery("SELECT w FROM SubjectData w", SubjectData.class);
        return query.getResultList();

    }

}
