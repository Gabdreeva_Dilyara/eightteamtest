package ru.itis.dao;


import ru.itis.hibernate.HibernateConnector;
import ru.itis.models.Settings;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class SettingDaoImpl extends SaveOrUpdateImpl <Settings, Long> implements CrudDao<Settings, Long>{

    private EntityManager entityManager;

    public SettingDaoImpl() {}

    @Override
    public Settings find(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Settings result =  entityManager.find(Settings.class, id);
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void delete(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Settings s = entityManager.find(Settings.class, id);
        entityManager.remove(s);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public List<Settings> findAll() {
        TypedQuery<Settings> query = entityManager.createQuery("SELECT w FROM Settings w", Settings.class);
        return query.getResultList();

    }
}
