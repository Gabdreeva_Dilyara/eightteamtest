package ru.itis.dao;

import ru.itis.hibernate.HibernateConnector;
import ru.itis.models.Subject;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public class SubjectDaoImpl extends SaveOrUpdateImpl<Subject, Long> implements CrudDao<Subject, Long>{

    private EntityManager entityManager;

    public SubjectDaoImpl() {}

    @Override
    public Subject find(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Subject result =  entityManager.find(Subject.class, id);
        entityManager.getTransaction().commit();
        return result;
    }

    @Override
    public void delete(Long id) {
        entityManager = HibernateConnector.getEntityManagerFactory().createEntityManager();
        entityManager.getTransaction().begin();
        Subject s = entityManager.find(Subject.class, id);
        entityManager.remove(s);
        entityManager.getTransaction().commit();
    }

    @Override
    public List<Subject> findAll() {
        TypedQuery<Subject> query = entityManager.createQuery("SELECT w FROM Subject w", Subject.class);
        return query.getResultList();

    }
}
