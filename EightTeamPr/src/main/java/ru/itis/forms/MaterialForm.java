package ru.itis.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class MaterialForm {
    private String header;
    private String content;
    private String path;
    private MultipartFile file;
}
