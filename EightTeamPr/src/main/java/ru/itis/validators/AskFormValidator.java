package ru.itis.validators;


import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import ru.itis.models.Question;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
@Component
public class AskFormValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(Question.class.getName());
    }

    @Override
    public void validate(Object o, Errors errors) {
        Question askForm = (Question) o;

    }
}
