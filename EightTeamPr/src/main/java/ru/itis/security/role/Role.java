package ru.itis.security.role;

/**
 * @author Dilyara Gabdreeva
 *         11-602
 *         <p>
 *         .2017
 */
public enum Role {
    ADMIN, USER
}
