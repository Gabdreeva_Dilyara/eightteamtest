<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Сайт для студентов</title>

  <link href="./css/style.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <script src="./js/jquery.min.js"></script>
  <script src="./js/jquery.dropotron.min.js"></script>
  <script type="text/javascript" src="./js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="./js/jquery.validate.min.js"></script>

  
</head>
<body>
  <!-- fixed navigation bar -->
  <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
      </div>
      
      <div class="collapse navbar-collapse" id="b-menu-1">
        <ul class="nav navbar-nav navbar-right">
        	
          <li><a href="/">Главная</a></li>
          <li><a href="/ask">Задать вопрос</a></li>
          <li><a href="/knowledge-base">База знаний</a></li>
          <li><a href="/rating">Рейтинг</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/profile">Моя страница</a></li>
              <li><a href="/setting">Настройки</a></li>
              <li><a href="/logout">Выйти</a></li>
            </ul>
          </li>
        </ul>
      </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
  </div> <!-- /.navbar -->

  <!-- 2-column layout -->
  <div class="container main-block">  
  	<div class="row">
      <div class="col-xs-12 col-sm-10 col-lg-8 col-md-offset-2 col-lg-offset-2">
       <div class="panel panel-primary">
        <div class="panel-heading">
         <h3 class="panel-title">
         Редактирование профиля</h3>
       </div>
       <div class="panel-body">
         <div class="row">
          <form class="q-form" id="registration" action="/setting" method="post">

            
            <#--<div class="col-xs-6 col-sm-6 col-md-6">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="Remember">
                  Показывать всем мои инициалы
                </label>
              </div>
            </div>

            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="checkbox">
                <label>
                  <input type="checkbox" value="Remember">
                  Показывать всем номер моей группы
                </label>
              </div>
            </div>-->

            <div class="form-group">
              <label for="q">Сменить логин:</label>
              <input class="form-control" name="login" id="login" class="q-field" type="text" required autofocus />
            </div>
            <div class="form-group">
              <label for="q">Сменить никнейм:</label>
              <input class="form-control" name="nickname" id="nickname" class="q-field" type="text" required autofocus />
            </div>
          </div>
          <div class="form-group">
            <label for="q">Сменить пароль:</label>
            <input class="form-control" name="password" id="password1" class="q-field" type="password" required autofocus />
          </div>
          <div class="form-group">
            <label for="q">Повторите пароль:</label>
            <input class="form-control" name="confirmPassword" id="password2" class="q-field" type="password" required autofocus />
          </div>
      </div>
      <div class="panel-footer">
       <div class="row">
         <div class="col-xs-6 col-sm-6 col-md-6">
            <input type="submit" class="btn btn-success" value="Сохранить изменения">
             </form>
        </div>
      </div>
    </div>

  </div>
</div>
</div>
</div>

<script type="text/javascript">
  var validator = $("#registration").validate({

    rules: {

      login: {
        minlength: 4,
        maxlength: 16,
        regx: /^[A-Za-z0-9_-]{4,16}$/
      },

      nickname: {
        minlength: 4,
        maxlength: 16,
        regx: /^[A-Za-z0-9_-]{4,16}$/
      },

      password1: {
        minlength: 3,
        maxlength: 16,
        regx: /^[A-Za-z0-9_-]{3,16}$/
      },

      password2: {
        equalTo: "#password1"
      }

    },

    messages: {

      login: {
        minlength: "Логин должен содержать минимум 4 символа",
        maxlength: "Логин должен содержать максимум 16 символов",
        regx: "Логин может содержать только буквы латинского алфавита, цифры, дефис и нижнее подчеркивание"
      },

      nickname: {
        minlength: "Никнейм должен содержать минимум 4 символа",
        maxlength: "Никнейм должен содержать максимум 16 символов",
        regx: "Никнейм может содержать только буквы латинского алфавита, цифры, дефис и нижнее подчеркивание"
      },

      password1: {
        minlength: "Пароль должен содержать минимум 3 символа",
        maxlength: "Пароль должен содержать максимум 16 символов",
        regx: "Пароль может содержать только буквы латинского алфавита, цифры, дефис и нижнее подчеркивание"
      },

      password2: {
        equalTo: "Пароли не совпадают"
      }

    }

  });

  $.validator.addMethod("regx", function(value, element, regexpr) {
    return regexpr.test(value);
  }, "Error");

  function registration() {
    validator.form();

    if (validator.numberOfInvalids() == 0) {
      location.href="index.html";
      alert("Изменения сохранены! Нажмите ОК для перехода на главную страницу.")
    }
    
  }

</script>

<footer>
  <!-- Footer -->
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>