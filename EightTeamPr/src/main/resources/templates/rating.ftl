<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Сайт для студентов</title>

  <link href="./css/style.css" rel="stylesheet">
  <!-- Bootstrap -->
  <link href="./css/bootstrap.min.css" rel="stylesheet">

  <script src="./js/jquery.min.js"></script>
  <script src="./js/jquery.dropotron.min.js"></script>
  <script type="text/javascript" src="./js/jquery-2.1.4.min.js"></script>
  <script type="text/javascript" src="./js/jquery.validate.min.js"></script>

  
</head>
<body>
  <!-- fixed navigation bar -->
  <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
      </div>
      
      <div class="collapse navbar-collapse" id="b-menu-1">
        <ul class="nav navbar-nav navbar-right">

          <#--<li>-->
            <#--<div id ="imaginary_container" class="input-group stylish-input-group">-->
              <#--<input type="text" class="form-control"  placeholder="Search student" >-->
              <#--<span class="input-group-addon">-->
               <#--<button type="submit">-->
                 <#--<span class="glyphicon glyphicon-search"></span>-->
               <#--</button>  -->
             <#--</span>-->
           <#--</div>-->
         <#--</li>-->
        	
          <li><a href="/">Главная</a></li>
          <li><a href="/ask">Задать вопрос</a></li>
          <li><a href="/knowledge-base">База знаний</a></li>
          <li class="active"><a href="/rating">Рейтинг</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
            <ul class="dropdown-menu">
            <li><a href="/profile">Моя страница</a></li>
              <li><a href="/setting">Настройки</a></li>
              <li><a href="/logout">Выйти</a></li>
            </ul>
          </li>
        </ul>
      </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
  </div> <!-- /.navbar -->

  <!-- 2-column layout -->
  <div class="container main-block">
  	<div class="row">
      <div class="col-xs-12 col-sm-10 col-lg-6 col-md-offset-3 col-lg-offset-3">
       <div class="panel panel-primary">
        <div class="panel-heading">
         <h3 class="panel-title">
         Рейтинг</h3>
       </div>
       <div class="panel-body">
         <div class="row">

          
          <#--<div class="col-sm-12">

            <div class="col-sm-8">
            <select class="form-control" name="subject" id="subject" class="q-field" type="text" required autofocus>
              <option>1 курс</option>
              <option>2 курс</option>
              <option>3 курс</option>
              <option>4 курс</option>
            </select>
          </div>
          <div class="col-sm-4">
            <a class="btn btn-default" href="#" role="button">Применить </a>
          </div>
          </div>-->


        
          <div class = "col-sm-12 rating">
            <div class="col-sm-2">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">1</h3>
                </div>
              </div>

            </div>

            <!-- /column 1 -->
            <div class="col-sm-8">

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Пользователь 1</h3>
                </div>
              </div>

            </div> <!-- /column 1 -->

          </div>
<#--
          <div class = "col-sm-12">
            <div class="col-sm-2">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">2</h3>
                </div>
              </div>

            </div>-->

           <#-- <!-- /column 2 &ndash;&gt;
            <div class="col-sm-8">

              <div class="panel panel-success">
                <div class="panel-heading">
                  <h3 class="panel-title">Пользователь 2</h3>
                </div>
              </div>

            </div> <!-- /column 2 &ndash;&gt;
          </div>-->

          <#--<div class = "col-sm-12">
            <div class="col-sm-2">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">3</h3>
                </div>
              </div>

            </div>

            <!-- /column 3 &ndash;&gt;
            <div class="col-sm-8">

              <div class="panel panel-warning">
                <div class="panel-heading">
                  <h3 class="panel-title">Пользователь 3</h3>
                </div>
              </div>

            </div> <!-- /column 3 &ndash;&gt;

          </div>-->

         <#-- <div class = "col-sm-12">
            <div class="col-sm-2">

              <div class="panel panel-primary">
                <div class="panel-heading">
                  <h3 class="panel-title">4</h3>
                </div>
              </div>

            </div>-->

            <!-- /column 4 -->
           <#-- <div class="col-sm-8">

              <div class="panel panel-default">
                <div class="panel-heading">
                  <h3 class="panel-title">Пользователь 4</h3>
                </div>
              </div>

            </div> <!-- /column 4 &ndash;&gt;-->
          </div>

        </div>
      </div>

    </div>
  </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>