<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Сайт для студентов</title>

    <link href="/../css/style.css" rel="stylesheet">
    <link href="/../css/bootstrap.min.css" rel="stylesheet">

    <script src="/../js/jquery.min.js"></script>
    <script src="/../js/jquery.dropotron.min.js"></script>
    <script src="/../js/jquery-2.1.4.min.js"></script>
    <script src="/../js/jquery.validate.min.js"></script>
</head>
<body>
  <!-- fixed navigation bar -->
  <div class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#b-menu-1">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Сайт студентов для студентов</a>
      </div>
      
      <div class="collapse navbar-collapse" id="b-menu-1">
        <ul class="nav navbar-nav navbar-right">

          <li><a href="/">Главная</a></li>
          <li><a href="/ask">Задать вопрос</a></li>
          <li><a href="/knowledge-base">База знаний</a></li>
          <li><a href="/rating">Рейтинг</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-user"></span><b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="/profile">Моя страница</a></li>
              <li><a href="/setting">Настройки</a></li>
              <li><a href="/logout">Выйти</a></li>
            </ul>
          </li>
        </ul>
      </div> <!-- /.nav-collapse -->
    </div> <!-- /.container -->
  </div> <!-- /.navbar -->

  <!-- 2-column layout -->
  <div class="container main-block">
    <div class="row row-offcanvas row-offcanvas-right">  
      <div class="row">
        <div class="col-xs-12 col-sm-10 col-lg-5">
         <div class="panel panel-primary">
          <div class="panel-heading">
           <h3 class="panel-title">О студенте</h3>
         </div>
         <div class="panel-body">
           <div class="row">
            <form class="q-form" id="registration" action="profile/{id}">
              <div class="form-group">
                <label for="q">Логин:</label>
                <label for="q">${model.user.login}</label>
              </div>
              <div class="form-group">
                <label for="q">Никнейм:</label>
                <label for="q">${model.user.nickname}</label>
              </div>
              <div class="form-group">
                <label for="q">Фамилия:</label>
                <label for="q">${model.user.secondName}</label>
              </div>
              <div class="form-group">
                <label for="q">Имя:</label>
                <label for="q">${model.user.firstName}</label>
              </div>
              <div class="form-group">
                <label for="q">Отчество:</label>
                <label for="q">${model.user.lastName}</label>
              </div>
              <div class="form-group">
                <label for="q">Баллы:</label>
                <label for="q">${model.user.score}</label>
              </div>
              <div class="form-group">
                <label for="q">Группа:</label>
                <label for="q">${model.user.userGroup}</label>
              </div>
            </form>
          </div>
        </div>
        <div class="panel-footer">
         <div class="row">
           <div class="col-xs-6 col-sm-6 col-md-6">
            <a href="/setting">
              <button type="button" class="btn btn-success">Редактировать информацию</button>
            </a>
          </div>
        </div>
      </div>
    </div>
  </div>


  <div class="col-xs-12 col-sm-10 col-lg-6">
   <div class="panel panel-primary">
    <div class="tabbable"> <!-- Only required for left/right tabs -->
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Мои вопросы</a></li>
        <li><a href="#tab2" data-toggle="tab">Мои ответы</a></li>
      </ul>
      <div class="tab-content">

        <div class="tab-pane active" id="tab1">
          <div class="panel-body">
            <div class="row">
              <#list model.questions as question>
                <#assign questionId>${question.questionId}</#assign>
              <form class="q-form" id="add_document" name="add_document" >
                <div class="form-group">
                  <li><a href="question/${questionId}">${question.header}</a></li>
                </div>
              </form>
              </#list>
            </div>
          </div>
        </div>

        <div class="tab-pane" id="tab2">
          <div class="panel-body">
            <div class="row">

              <form class="q-form" id="add_link" name="add_link">

                <div class="form-group">
                  <li><a href="#">Ответ 1</a></li>
                </div>
                <div class="form-group">
                  <li><a href="#">Ответ 2</a></li>
                </div>
                <div class="form-group">
                  <li><a href="#">Ответ 3</a></li>
                </div>
                <div class="form-group">
                <li><a href="#">Ответ 4</a></li>
              </div>
              <div class="form-group">
                <li><a href="#">Ответ 5</a></li>
              </div>
              <div class="form-group">
                <li><a href="#">Ответ 6</a></li>
              </div>
            </form>

          </div>

        </div>
      </div>

    </div>
  </div>
</div>
</div>
</div>
</div>
</div>



<footer>
  <!-- Footer -->
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/../js/bootstrap.min.js"></script>
</body>
</html>