<#ftl encoding='UTF-8'>
<!DOCTYPE html>
<html>
<head>
    <title>Сайт для студентов</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="/../css/style.css" rel="stylesheet">
    <link href="/../css/auth.css" rel="stylesheet">
    <link href="/../css/bootstrap.min.css" rel="stylesheet">


    <script src="/../js/jquery.min.js"></script>
    <script src="/../js/jquery.dropotron.min.js"></script>
    <script src="/../js/jquery-2.1.4.min.js"></script>
    <script src="/../js/jquery.validate.min.js"></script>

</head>
<body>
<div class="container main-block2">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-lg-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Авторизация</h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <form role="form" class="auth-form" id="insert_form" action="/login" method="POST">
                        <#if model.error??>
                            <div class="alert alert-danger " role="alert">${model.error}</div>
                        </#if>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                                <input name="login" type="text" class="form-control" placeholder="Логин" required autofocus />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                                <input name="password" type="password" class="form-control" placeholder="Пароль" required />
                            </div>
                            <p>
                                <#--<a href="#">Забыли свой пароль?</a></p>-->
                            У вас нет аккаунта? <a href="/signUp">Зарегистрироваться</a>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                    <#-- <div class="col-xs-6 col-sm-6 col-md-6">
                         <#-v class="checkbox">
                             <label>
                                 <input type="checkbox" value="Remember">
                                 Запомнить меня
                             </label>
                         </div>
                     </div>--->
                        <button type="submit" class="btn btn-labeled btn-success">
                        <#--<span class="btn-label"><i class="glyphicon glyphicon-ok"></i></span>-->Войти</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-lg-7">
            <div class="panel panel-primary slider-div">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        О нас</h3>
                </div>
                <!-- slider -->
                <div id="slider" class="carousel slide" data-ride="carousel">
                    <!-- controls -->
                    <ol class="carousel-indicators">
                        <li data-target="#slider" data-slide-to="0" class="active"></li>
                        <li data-target="#slider" data-slide-to="1"></li>
                        <li data-target="#slider" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <!-- slides -->
                        <div class="item active">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Lorem ipsum - 1</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sit amet tempus massa. Nam quis purus sit amet augue iaculis dapibus non in nisi.<br />Sed sed volutpat neque. Nulla posuere.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Lorem ipsum - 2</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sit amet tempus massa. Nam quis purus sit amet augue iaculis dapibus non in nisi.<br />Sed sed volutpat neque. Nulla posuere.</p>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="container">
                                <div class="carousel-caption">
                                    <h1>Lorem ipsum - 3</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In sit amet tempus massa. Nam quis purus sit amet augue iaculis dapibus non in nisi.<br />Sed sed volutpat neque. Nulla posuere.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- left-right controls -->
                    <a class="left carousel-control" href="#slider" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                    <a class="right carousel-control" href="#slider" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                </div><!-- /.carousel -->
            </div>
        </div> V
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>