create sequence hibernate_sequence
;

create table hibernate_sequences
(
	sequence_name varchar(255) not null
		constraint hibernate_sequences_pkey
			primary key,
	next_val bigint
)
;

create table answer
(
	answer_id bigint not null
		constraint answer_pkey
			primary key,
	content varchar(255),
	datetime timestamp,
	file varchar(255),
	rating integer,
	question bigint,
	author bigint
)
;

create table question
(
	question_id bigint not null
		constraint question_pkey
			primary key,
	complaintscount integer,
	content varchar(255),
	datetime timestamp,
	file varchar(255),
	header varchar(255),
	subject bigint,
	author bigint
)
;

alter table answer
	add constraint fks4yfxjrfvek48fcsvg9ndcktu
		foreign key (question) references question
;

create table subject
(
	subject_id bigint not null
		constraint subject_pkey
			primary key,
	name varchar(255),
	eng_name varchar(255)
)
;

alter table question
	add constraint fkofbh84oh3lxcaboy6d5itpdrf
		foreign key (subject) references subject
;

create table subject_data
(
	data_id bigint not null
		constraint subject_data_pkey
			primary key,
	content varchar(255),
	rating integer,
	type boolean,
	subject bigint
		constraint fk7p4ovuf8xj3eov3vcmcfgamwr
			references subject,
	path varchar(255),
	header varchar(255)
)
;

create table users
(
	users_id bigint not null
		constraint users_pkey
			primary key,
	login varchar(255),
	nickname varchar(255),
	password varchar(255),
	role varchar(255),
	first_name varchar(50),
	second_name varchar(50),
	last_name varchar(50),
	score smallint default 0,
	user_group varchar(15)
)
;

alter table answer
	add constraint fka80smbsrt45l8yqsp4v4ellyq
		foreign key (author) references users
;

alter table question
	add constraint fk1ov3cws4kseq63i5s3np3k72u
		foreign key (author) references users
;

